.. -*- coding: utf-8; indent-tabs-mode:nil; -*-
.. SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ@zougloub.eu>
.. SPDX-License-Identifier: MIT

#############################
XDG Shared MIME Info Packages
#############################

This is a collection of random packages allowing to detect file
`media type`_, based on the file contents (`content sniffing`_)
), using the `XDG Shared MIME Info infrastructure`_.

.. _media type:
   https://en.wikipedia.org/wiki/Media_type

.. _content sniffing:
   https://en.wikipedia.org/wiki/Content_sniffing

.. _XDG Shared MIME Info infrastructure:
   http://freedesktop.org/Software/shared-mime-info

Most of the standard packages out there are first considering
extensions. These packages are not mature enough or QA-tested enough
to be submitted for upstreaming.


Background
##########

A media type (also known as a Multipurpose Internet Mail Extensions or
MIME type) is a standard that indicates the nature and format of a
document, file, or assortment of bytes. It is defined and standardized
in IETF's :rfc:`6838`.

Software that can infer media types:

- `libmagic` and its `file` cli.

  `libmagic` uses a magic database.

  https://linux.die.net/man/5/magic

- Things based on `XDG Shared MIME Info infrastructure`_.

  See
  http://freedesktop.org/wiki/Specifications/shared-mime-info-spec

- Probably others.


The `XDG Shared MIME Info infrastructure`_ provides extensibility,
and compiled from reasonably well specified sources, and these
packages are augmenting it.


Installation
############

Have files into `~/.local/share/mime/packages` or the system location.

For example:

.. code:: sh

   cd ~/.local/share/mime/packages
   for file in ~/.local/opt/cJ/xdg-shared-mime-info-packages/*.xml; do
     ln -sfr "$file" cJ-$(basename "$file");
   done

Then update the MIME database:

.. code:: sh

   update-mime-database ~/.local/share/mime


Usage
#####



Then you can recognize a supported file's MIME type doing

.. code:: sh

   mimetype ${file}


